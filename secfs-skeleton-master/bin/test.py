# test
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding

import secfs.crypto
from secfs.types import User

if __name__ == '__main__':
    user = User(10)
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )
    public_key = private_key.public_key()
    bad_private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )
    bad_public_key = bad_private_key.public_key()
    data = b"hello crypto"
    encrypted = public_key.encrypt(
        data,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )
    decrypted = private_key.decrypt(
        encrypted,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )
    print("data={}".format(data))
    print("encrypted={}".format(encrypted))
    print("decrypted={}".format(decrypted))
    sign = secfs.crypto.sign(private_key, data)
    if secfs.crypto.verify(sign, public_key, data):
        print("success")
    if secfs.crypto.verify(sign, bad_public_key, data):
        print("wrong")
