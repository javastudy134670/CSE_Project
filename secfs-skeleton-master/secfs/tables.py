# This file contains all code handling the resolution and modification of i
# mappings. This includes group handle indirection and VSL validation, so the
# file is somewhat hairy.
# NOTE: an ihandle is the hash of a principal's itable, which holds that
# principal's mapping from inumbers (the second part of an i) to inode hashes.

import pickle
from typing import Dict, Optional

import secfs.fs
import secfs.vs
import secfs.store.block
import secfs.crypto
from secfs.vs import VS, VSL
from secfs.types import I, Principal, User

# a server connection handle is passed to us at mount time by secfs-fuse
server = None


def register(_server):
    global server
    server = _server


class Itable:
    """
    An itable holds a particular principal's mappings from inumber (the second
    element in an i tuple) to an inode hash for users, and to a user's i for
    groups.
    """

    def __init__(self):
        self.mapping = {}

    def bytes(self):
        return pickle.dumps(self.mapping)

    @staticmethod
    def load(ihandle: str):
        b = secfs.store.block.load(ihandle)
        if b is None:
            return None

        t = Itable()
        t.mapping = pickle.loads(b)
        return t


# current_itables represents the current view of the file system's itables
current_itables: Dict[Principal, Itable] = {}
# Luo: modification+itables represents the changes
update_itables: Dict[Principal, Itable] = {}
# most recent vs in this client
last_vs: Optional[VS] = None


def resolve(i, resolve_groups=True):
    """
    Resolve the given i into an inode hash. If resolve_groups is not set, group
    is will only be resolved to their user i, but not further.

    In particular, for some i = (principal, inumber), we first find the itable
    for the principal, and then find the inumber-th element of that table. If
    the principal was a user, we return the value of that element. If not, we
    have a group i, which we resolve again to get the ihash set by the last
    user to write the group i.
    """
    if not isinstance(i, I):
        raise TypeError("{} is not an I, is a {}".format(i, type(i)))

    principal = i.p

    if not isinstance(principal, Principal):
        raise TypeError("{} is not a Principal, is a {}".format(principal, type(principal)))

    if not i.allocated():
        # someone is trying to look up an i that has not yet been allocated
        return None

    global current_itables
    if principal not in current_itables:
        # User does not yet have an itable
        return None

    t = current_itables[principal]

    if i.n not in t.mapping:
        raise LookupError("principal {} does not have i {}".format(principal, i))

    # santity checks
    if principal.is_group() and not isinstance(t.mapping[i.n], I):
        raise TypeError("looking up group i, but did not get indirection ihash")
    if principal.is_user() and isinstance(t.mapping[i.n], I):
        raise TypeError("looking up user i, but got indirection ihash")

    if isinstance(t.mapping[i.n], I) and resolve_groups:
        # we're looking up a group i
        # follow the indirection
        return resolve(t.mapping[i.n])

    return t.mapping[i.n]


def modmap(mod_as, i, ihash):
    """
    Changes or allocates i so it points to ihash.

    If i.allocated() is false (i.e. the I was created without an i-number), a
    new i-number will be allocated for the principal i.p. This function is
    complicated by the fact that i might be a group i, in which case we need
    to:

      1. Allocate an i as mod_as
      2. Allocate/change the group i to point to the new i above

    modmap returns the mapped i, with i.n filled in if the passed i was no
    allocated.
    """
    if not isinstance(i, I):
        raise TypeError("{} is not an I, is a {}".format(i, type(i)))
    if not isinstance(mod_as, User):
        raise TypeError("{} is not a User, is a {}".format(mod_as, type(mod_as)))

    assert mod_as.is_user()  # only real users can mod

    if mod_as != i.p:
        print("trying to mod object for {} through {}".format(i.p, mod_as))
        assert i.p.is_group()  # if not for self, then must be for group

        real_i = resolve(i, False)
        if isinstance(real_i, I) and real_i.p == mod_as:
            # We updated the file most recently, so we can just update our i.
            # No need to change the group i at all.
            # This is an optimization.
            i = real_i
        elif isinstance(real_i, I) or real_i is None:
            if isinstance(ihash, I):
                # Caller has done the work for us, so we just need to link up
                # the group entry.
                print("mapping", i, "to", ihash, "which again points to", resolve(ihash))
            else:
                # Allocate a new entry for mod_as, and continue as though ihash
                # was that new i.
                # XXX: kind of unnecessary to send two VS for this
                _ihash = ihash
                ihash = modmap(mod_as, I(mod_as), ihash)
                print("mapping", i, "to", ihash, "which again points to", _ihash)
        else:
            # This is not a group i!
            # User is trying to overwrite something they don't own!
            raise PermissionError("illegal modmap; tried to mod i {0} as {1}".format(i, mod_as))

    # find (or create) the principal's itable
    t: Itable
    global current_itables
    if i.p not in current_itables:
        if i.allocated():
            # this was unexpected;
            # user did not have an itable, but an inumber was given
            raise ReferenceError("itable not available")
        t = Itable()
        print("no current list for principal", i.p, "; creating empty table", t.mapping)
    else:
        t = current_itables[i.p]

    # look up (or allocate) the inumber for the i we want to modify
    if not i.allocated():
        inumber = 0
        while inumber in t.mapping:
            inumber += 1
        i.allocate(inumber)
    else:
        if i.n not in t.mapping:
            raise IndexError("invalid inumber")

    # modify the entry, and store back the updated itable
    if i.p.is_group():
        print("mapping", i.n, "for group", i.p, "into", t.mapping)
    t.mapping[i.n] = ihash  # for groups, ihash is an i
    current_itables[i.p] = t
    update_itables[i.p] = t
    return i


def pre(refresh, user: User, to_verify: bool):
    """
    Called before all user file system operations, right after we have obtained
    an exclusive server lock.
    """
    global server, current_itables, last_vs
    last_vs = VS(user)
    vsl_hash = server.pull_vsl_hash()
    if vsl_hash is None:
        return
    vsl = VSL.load(vsl_hash)
    current_itables.clear()
    if to_verify:
        print("verify vs from user {}".format(vsl.recent_vs.user))
        pubkey = secfs.fs.usermap[vsl.recent_vs.user]
        assert vsl.recent_vs.verify(pubkey), "error happened while verify version structure"
    assert vsl.recent_vs.later_than(last_vs), "captured a fork attack"
    last_vs.vnumbers.update(vsl.recent_vs.vnumbers)
    for p, i_handle in vsl.i_handles.items():
        current_itables[p] = Itable.load(i_handle)
    if refresh is not None:
        # refresh usermap and groupmap
        refresh()


def post():
    vsl_hash_fetch = server.pull_vsl_hash()
    if vsl_hash_fetch is None:
        vsl = VSL(last_vs)
    else:
        vsl = VSL.load(vsl_hash_fetch)
    for p, itable in update_itables.items():
        i_handle = secfs.store.block.store(itable.bytes())
        vsl.i_handles[p] = i_handle
    update_itables.clear()
    user = last_vs.user
    if user in last_vs.vnumbers.keys():
        last_vs.vnumbers[user] += 1
    else:
        last_vs.vnumbers[user] = 0
    print("sign as user {}".format(user))
    prikey = secfs.crypto.keys[user]
    last_vs.sign(prikey)
    vsl.recent_vs = last_vs
    vsl_hash_push = secfs.store.block.store(vsl.bytes())
    server.push_vsl_hash(vsl_hash_push)
    pass
