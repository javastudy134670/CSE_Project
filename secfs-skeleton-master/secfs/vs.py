import pickle

import secfs.store.block
from typing import Dict
from secfs.types import User, Principal
import secfs.crypto


class VS:

    def __init__(self, user: User):
        self.user: User = user
        self.signs = None
        self.changes: Dict[Principal, str] = {}
        self.vnumbers: Dict[Principal, int] = {}

    def bytes(self) -> bytes:
        return pickle.dumps(self)

    @staticmethod
    def load(vs_hash: str):
        b = secfs.store.block.load(vs_hash)
        vs: VS = pickle.loads(b)
        return vs

    def verify(self, pubkey) -> bool:
        return secfs.crypto.verify(self.signs, pubkey, pickle.dumps(self.vnumbers))

    def sign(self, prikey):
        self.signs = secfs.crypto.sign(prikey, pickle.dumps(self.vnumbers))

    def later_than(self, other_vs) -> bool:
        assert isinstance(other_vs, VS)
        for p in self.vnumbers.keys():
            if p in other_vs.vnumbers.keys() and other_vs.vnumbers[p] > self.vnumbers[p]:
                return False
        return True

    pass


class VSL:

    def __init__(self, recent_vs: VS):
        self.recent_vs = recent_vs
        self.i_handles: Dict[Principal, str] = {}

    @staticmethod
    def load(vsl_hash: str):
        b = secfs.store.block.load(vsl_hash)
        vsl: VSL = pickle.loads(b)
        return vsl

    def bytes(self) -> bytes:
        return pickle.dumps(self)
